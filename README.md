# Baldur's Gate 3 DPS Simulation

This program simulates DPS for various Baldur's Gate 3 class combinations.

This program reads .json style class definitions from the resources directory and sub directories and prints a table of the results, along with a results.csv file.

This program currently is intended to for BG3 and leverages certain rule changes not present in DnD 5e paper rules
Notable examples include being able to 'stealth' as a bonus action to gain advantage and being able to apply sneak attack damage with str-based attacks (if the weapon is finesse).

## Limitations ##
This simulation doesn't take into account spells, aside from Hunter's Mark, Hex, and Paladin Smites.
Lots of subclasses are missing.
Lots of feats missing.
No simulation for most caster classes

## Supported Features
- Classes: Fighter, Rogue, Paladin, Ranger, Barbarian
- Sub-classes: Champion, Thief, Oath-Of-Vengeance, Gloom-stalker, Berserker
- Races: Half-Orc and Halfling
- A variety of ranged and melee weapons
- Simulating damage across a range of target armor values
- Average chance to hit/crit/miss are factored into expected DPS.
- Supports great-weapon-master and sharpshooter.
- Chance to hit/crit/miss per attack accounting for expanded crit range, advantage, and halfling-luck re-rolls.

## Result Fields Explanation
- build - I use a shorthand to describe the weapons, class levels, feats, and race.
- burst - The average expected damage from a round of combat allowing for burst cooldowns but requiring buffs/debuffs.
- prep - A round of combat without burst cooldowns, and accounting for having to buff with Rage/Hunter's Mark/Hex if applicable
- regular - Expected sustained DPS w/o burst cooldowns, but with buffs/debuffs already present.
- 0-cost - Expected damage if NO resource costing abilities are available. E.g. no spell slots or rage.
- avg - The average damage from burst, prep, and regular.
- Min_AC - The start of the simulated target AC range
- Max_AC - The last simulated target AC value
- +hit - The sum of all values to the build's hit bonus
- AHit% - The average chance to land a hit across the range of ACs simmed
- AMiss% - Average chance to miss on an attack
- crit% - The chance the user will crit.


## Assumptions
- Because BG3 allows Stealth as a bonus action, it is assumed that ranged attackers will be able to stealth for their attacks.
- All weapons simmed here are assumed at +3.

## Major BG3 Changes accounted for here
1) All classes can stealth as a bonus action, making ranged advantage very easy to achieve. This was true for EA, it is possible this will be changed in the final release and the sim will need to be updated.
2) 'Thief' level 3 rogue subclass grants an extra bonus action, which is a large damage increase if it can be used for an extra attack.
3) Berserker Barbarian rage does not grant a level of exhaustion, so it is a great class pickup to convert bonus actions to extra attacks and gain melee advantage.


## Shorthands
- 1HX - Hand-crossbow
- 2LB - longbow
- 2XB - heavy-crossbow
- 2HS - greatsword
- 2HA - greataxe
- 2HP - polearm
- 1HR - rapier
- DWR - dual wield rapiers
- DWS - dual wield shortswords
- GWM - great-weapon-master
- SS - sharpshooter
- PW_AC - "Power Wasted AC". The AC value where GWM/SS will reduce damage.

### Class shorthands
- Fi - Fighter
- Ra - Ranger
- Pa - Paladin
- Ba - Barbarian
- Ro - Rogue
- Wl - Warlock

### Races shorthand
- orc - Half-orc
- hl - halfling
