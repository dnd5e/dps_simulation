import unittest

from constants import RANGED_WEAPONS
from simulate_dps import *


def get_build(target_ac=0, hit_bonus=0, weapon_type="club", race="human", subclasses=None, feats=None):
    if feats is None:
        feats = []

    if subclasses is None:
        subclasses = []

    sub_type = "melee"
    if weapon_type in RANGED_WEAPONS:
        sub_type = "ranged"

    weapon_1 = {
        "type": weapon_type,
        "sub_type": sub_type
    }

    return {
        "target_ac": target_ac,
        "race": race,
        "hit_bonus": hit_bonus,
        "weapon_1": weapon_1,
        "subclasses": subclasses,
        "feats": feats
    }


class TestMechanics(unittest.TestCase):

    def test_miss_chance(self):
        build = get_build(target_ac=100)
        assert get_miss_chance(build, has_advantage=False) == .95
        build = get_build(target_ac=-100)
        assert get_miss_chance(build, has_advantage=False) == .05

        build = get_build(target_ac=100)
        assert get_miss_chance(build, has_advantage=True) == .9025
        build = get_build(target_ac=-100)
        assert get_miss_chance(build, has_advantage=True) == (1 / 400)

        build = get_build(target_ac=100, weapon_type="greatsword", race="halfling")
        assert get_miss_chance(build, has_advantage=False) == .9475  # 1-(.05 + (.05*.05))
        build = get_build(target_ac=-100, weapon_type="greatsword", race="halfling")
        assert get_miss_chance(build, has_advantage=False) == (1 / 400)

        build = get_build(target_ac=100, weapon_type="greatsword", race="halfling", subclasses=["champion"])
        assert get_miss_chance(build, has_advantage=False) == .895  # 1 - (.1 + .05 * .1)
        build = get_build(target_ac=-100, weapon_type="greatsword", race="halfling", subclasses=["champion"])
        assert get_miss_chance(build, has_advantage=False) == (1 / 400)

        # Lucky /w Advantage on edge cases
        build = get_build(target_ac=100, race="halfling")
        assert get_miss_chance(build, has_advantage=True) == 0.89775625
        build = get_build(target_ac=-100, race="halfling")
        assert get_miss_chance(build, has_advantage=True) == (1 / 160000)

        build = get_build(target_ac=100, race="halfling", subclasses=["champion"])
        assert get_miss_chance(build, has_advantage=True) == .801025  # (0.895)**2
        build = get_build(target_ac=-100, race="halfling", subclasses=["champion"])
        assert get_miss_chance(build, has_advantage=True) == .00000625  # round((1-(.95+.05*.95))**2, 10)

    def test_crit_chance(self):
        # Regular 5% crit chance
        build = get_build()
        assert get_crit_chance(build, has_advantage=False) == .05

        # Test Advantage
        build = get_build()
        assert get_crit_chance(build, has_advantage=True) == .0975

        # Test Lucky
        build = get_build(weapon_type="club", race="halfling")
        assert get_crit_chance(build, has_advantage=False) == .0525

        # Lucky /w Advantage on edge cases
        build = get_build(race="halfling")
        assert get_crit_chance(build, has_advantage=True) == 0.10224375
        # 0.10224375

        # Expanded Crit Range
        # Regular 5% crit chance
        build = get_build(subclasses=["champion"])
        assert get_crit_chance(build, has_advantage=False) == .1

        # Test Advantage
        build = get_build(subclasses=["champion"])
        assert get_crit_chance(build, has_advantage=True) == .19

        # Test Lucky
        build = get_build(race="halfling", subclasses=["champion"])
        assert get_crit_chance(build, has_advantage=False) == .105

        # Lucky /w Advantage on edge cases
        build = get_build(race="halfling", subclasses=["champion"])
        assert get_crit_chance(build, has_advantage=True) == 0.198975


if __name__ == '__main__':
    unittest.main()
