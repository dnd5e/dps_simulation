LIGHT_WEAPONS = {
    "scimitar",
    "shortsword",
    "dagger"
}

FINESSE_WEAPONS = {
    "rapier",
    "scimitar",
    "shortsword",
    "dagger",
    "dart",
}

TWO_HANDED_WEAPONS = {
    "greatsword",
    "greataxe",
    "polearm",
    "greatclub",
    "glaive",
    "halberd",
    "maul",
    "pike",
}

VERSATILE_WEAPONS = {
    "quarterstaff",
    "spear",
    "battleaxe",
    "longsword",
    "warhammer"
}

RANGED_WEAPONS = {
    "longbow",
    "crossbow",
    "hand-crossbow",
    "heavy-crossbow",
    "eldritch-blast"
}

MARTIAL_CLASSES = {
    "fighter",
    "ranger",
    "paladin",
    "barbarian"
}

KNOWN_ELDRITCH_EVOCATIONS = {
    "thirsting-blade"  # requires warlock lvl 5, allows for an extra attack
}

KNOWN_SUBCLASS = {
    "champion": "fighter",  # gives improved crit range
    "gloom-stalker": "ranger",  # allows 1 burst round attack
    "thief": "rogue",  # allows 2x bonus actions
    "college-of-swords": "bard",  # lvl 3 fighting style, lvl 6 extra attack
    "berserker": "barbarian",  # use bonus action for MH attacks
    "oath-of-vengeance": "paladin",  # allows hunter's mark
    "the-fiend": "warlock",  # no simulated affect
    "war-domain": "cleric",
    "wild-magic-barbarian": "barbarian"
}

KNOWN_FEATS = {
    "polearm-mastery",  # allows bonus attack with a polearm
    "crossbow-expert",  # allows bonus attacks with a hand crossbow
    "sentinel",  # not used in this simulation
    "sharpshooter",  # -5 to hit, +10 to damage
    "great-weapon-master",  # -5 to hit, +10 to damage
    "savage-attacker",  # advantage on 1 weapon attack damage roll per turn
    "dual-wielder",
    "spell-sniper"
}

KNOWN_FIGHTING_STYLES = {
    "archery",  # provides +2 to hit with ranged weapons
    "great-weapon-fighting",  # higher expected damage for great weapons
    "two-weapon-fighting",  # Add ability mod to offhand damage # TODO: account for dual wield melee weapons
    "defense",  # +1 AC
    "protection",  # not used here
    "dueling"  # +2 damage for empty offhand and non-heavy melee main-hand
}

WEAPON_DICE_MAP = {
    "eldritch-blast": "1d10",
    "heavy-crossbow": "1d10",
    "crossbow": "1d8",
    "hand-crossbow": "1d6",
    "longbow": "1d8",
    "greatsword": "2d6",
    "polearm": "1d10",
    "greataxe": "1d12",
    "greatclub": "1d8",
    "glaive": "1d10",
    "halberd": "1d10",
    "maul": "2d6",
    "pike": "1d10",
    "quarterstaff": "1d6",
    "spear": "1d6",
    "battleaxe": "1d8",
    "longsword": "1d8",
    "warhammer": "1d8",
    "rapier": "1d8",
    "scimitar": "1d6",
    "shortsword": "1d6",
    "dagger": "1d4",
    "dart": "1d4"
}

PROFICIENCY_MAP = {
    1: 2,
    2: 2,
    3: 2,
    4: 2,
    5: 3,
    6: 3,
    7: 3,
    8: 3,
    9: 4,
    10: 4,
    11: 4,
    12: 4,
    13: 5,
    14: 5,
    15: 5,
    16: 5,
    17: 6,
    18: 6,
    19: 6,
    20: 6,
}
