import math
import os
import json
import pandas as pd
from tabulate import tabulate

from constants import MARTIAL_CLASSES, PROFICIENCY_MAP, WEAPON_DICE_MAP, KNOWN_SUBCLASS, KNOWN_FEATS, \
    KNOWN_FIGHTING_STYLES, TWO_HANDED_WEAPONS, VERSATILE_WEAPONS, FINESSE_WEAPONS, LIGHT_WEAPONS


def empty_offhand(build: dict) -> bool:
    weapon_2 = build.get("weapon_2", {})
    if weapon_2 and len(weapon_2) > 0:
        return False
    return True


def get_expected_feats(build: dict):
    class_levels = build['class_levels']
    feat_count = 0
    for class_name in class_levels:
        class_level = class_levels[class_name]
        if class_level >= 4:
            feat_count += 1
        if class_level >= 8:
            feat_count += 1
        if class_level >= 12:
            feat_count += 1
        if class_level >= 16:
            feat_count += 1
        if class_level >= 19:
            feat_count += 1
    fighter_levels = get_class_level(build, "fighter")
    if fighter_levels >= 6:
        feat_count += 1
    if fighter_levels >= 14:
        feat_count += 1
    rogue_levels = get_class_level(build, "rogue")
    if rogue_levels >= 10:
        feat_count += 1
    return feat_count


def get_expected_fighting_styles(build: dict):
    expected_fighting_styles = 0
    fighter_level = get_class_level(build, "fighter")
    subclasses = build['subclasses']
    if fighter_level > 0:
        expected_fighting_styles += 1
    if fighter_level >= 10 and 'champion' in subclasses:
        expected_fighting_styles += 1

    paladin_level = get_class_level(build, "paladin")
    if paladin_level >= 2:
        expected_fighting_styles += 1

    ranger_level = get_class_level(build, "ranger")
    if ranger_level >= 2:
        expected_fighting_styles += 1

    bard_level = get_class_level(build, "bard")
    if bard_level >= 3 and "college-of-swords" in subclasses:
        expected_fighting_styles += 1

    return expected_fighting_styles


def light_offhand(build):
    if empty_offhand(build):
        return False
    weapon = build['weapon_2']
    weapon_type = weapon['type']
    return weapon_type in LIGHT_WEAPONS


def get_expected_subclasses(build: dict):
    expected_subclasses = 0
    class_levels = build['class_levels']

    for _, class_level in class_levels.items():
        if class_level >= 3:  # level 3 grants subclass
            expected_subclasses += 1
    cleric_level = get_class_level(build, "cleric")
    if 1 <= cleric_level < 3:
        expected_subclasses += 1
    warlock_level = get_class_level(build, "warlock")
    if 1 <= warlock_level < 3:
        expected_subclasses += 1
    return expected_subclasses


def validate_build(build: str, build_name: str = None, expected_level: int = None) -> dict:
    """
    Throw errors indicating problems with the character json
    :param expected_level:
    :param build_name: Name of this class build
    :param build:
    :return:
    """
    errors = []
    try:

        build = json.loads(build)
        build['build_name'] = build_name
        assert "class_levels" in build
        class_levels = build['class_levels']
        character_level = sum(class_levels.values())
        assert character_level > 0 & character_level <= 20
        if expected_level is not None and expected_level != character_level:
            errors.append(f"Character level:{character_level} different than expected level:{expected_level}")

        less_validation = build.get("less_validation", False)
        if not less_validation:
            warlock_levels = get_class_level(build, "warlock")
            ranger_levels = get_class_level(build, "ranger")
            if has_rage(build):
                if (0 < warlock_levels < 3) or (0 < ranger_levels < 3):
                    errors.append("Levels in Barbarian and 1/2 levels in warlock/ranger. Rage prevents hex/mark. "
                                  "Add 'less_validation' True to skip this warning.")
            if has_loading(build) and "crossbow-expert" not in get_feats(build):
                errors.append("Leveraging a crossbow weapon without crossbow-expert feat!")

        expected_subclasses = get_expected_subclasses(build)
        subclasses = build.get("subclasses", [])

        if len(subclasses) != expected_subclasses:
            errors.append(f"Expected # of subclasses is {expected_subclasses}. {len(subclasses)} present.")

        for subclass in subclasses:
            if subclass not in KNOWN_SUBCLASS.keys():
                errors.append(f"Subclass:{subclass} is not a known_subclass!")
            required_class = KNOWN_SUBCLASS[subclass]
            class_level = get_class_level(build, required_class)
            required_level = 3
            if required_class == "warlock" or required_class == "cleric":
                required_level = 1
            if class_level < required_level:
                errors.append(f"Subclass:{subclass} requires {required_class}:3. current class level:{class_level}")

        feats = build.get("feats", [])
        for feat in feats:
            if feat not in KNOWN_FEATS:
                errors.append(f"Feat:{feat} is not a known feat!")

        if not empty_offhand(build) and not light_offhand(build):
            if "dual-wielder" not in feats:
                errors.append("Offhand isn't light, and dual-wielding feat isn't taken.")

        expected_feats = get_expected_feats(build)
        required_feats = max(build['main_stat_mod'] - 3, 0)
        required_feats += len(feats)

        if required_feats > expected_feats:
            errors.append(f"Not enough ASIs for stats/feats! Required:{required_feats} Available:{expected_feats}")
        if expected_feats > required_feats:
            errors.append(f"{expected_feats} ASIs available, but only using {required_feats}")

        fighting_styles = build.get("fighting_styles", [])
        for fighting_style in fighting_styles:
            if fighting_style not in KNOWN_FIGHTING_STYLES:
                errors.append(f"Fighting Style:{fighting_style} is not a known fighting style!")

        expected_fighting_styles = get_expected_fighting_styles(build)
        if len(fighting_styles) != expected_fighting_styles:
            errors.append(
                f"{len(fighting_styles)} fighting style provided; but build allows for {expected_fighting_styles}.")

    except Exception as error:
        errors.append(error)
    if len(errors) > 0:
        print(f"Errors occurred when validating class file:{build_name}!")
        raise Exception(errors)
    return build


def get_character_level(build: dict):
    return sum(build['class_levels'].values())


def get_class_level(build, class_name):
    return build.get("class_levels", {}).get(class_name, 0)


def get_bonus_weapon(build: dict):
    weapon_1 = build['weapon_1']
    if not empty_offhand(build):
        return build['weapon_2']
    if weapon_1['type'] == 'polearm':
        return {  # Return special polearm bonus attack
            "type": "polearm",
            "sub_type": "melee",
            "modifier": weapon_1['modifier'],
            "dice": "1d4"
        }
    return weapon_1


def get_mh_attacks(build: dict):
    """
    Returns the number of attacks that will be done using the attack action
    :param build:
    :return:
    """
    if build['weapon_1']['type'] == "eldritch-blast":
        level = get_character_level(build)
        if level >= 17:
            return 4
        if level >= 11:
            return 3
        if level >= 5:
            return 2
        return 1

    if get_class_level(build, "fighter") == 20:
        return 4
    if get_class_level(build, "fighter") >= 11:
        return 3
    for martial_class in MARTIAL_CLASSES:
        if get_class_level(build, martial_class) >= 5:
            return 2
    bard_level = get_class_level(build, "bard")
    if bard_level >= 6 and "college-of-swords" in build.get("subclasses", []):
        return 2

    if "thirsting-blade" in build.get("eldritch_evocations", []):
        return 2

    return 1  # minimum number of MH attacks


def get_bonus_attacks(build: dict, no_resources=False):
    """
    :param no_resources: Set to 'true' if no consumed resources allowed
    :param build: Contains all fields from the build json file to calculate DPS
    :return: Returns the number of bonus attacks the class will do
    """
    weapon_1 = build['weapon_1']
    feats = set(get_feats(build))

    bonus_attacks = 0
    if weapon_1['type'] == "polearm" and 'polearm-mastery' in feats:
        bonus_attacks = 1
    if weapon_1['type'] == "hand-crossbow" and 'crossbow-expert' in feats:
        bonus_attacks = 1

    # If weapon 2 is melee; we can bonus attack with it
    if is_melee(build, weapon=2):
        bonus_attacks = 1

    if 'berserker' in build['subclasses'] and not no_resources:
        bonus_attacks = 1

    # In BG3, thieves get 2 bonus actions
    if "thief" in build['subclasses'] and build['engine'] == "bg3":
        bonus_attacks = bonus_attacks * 2
    return bonus_attacks


def apply_great_weapon_fighting(dice_size: int):
    gwf_chance = (dice_size - 2) / dice_size
    gwf_avg = (3 + dice_size) / 2
    avg_chance = 1 - gwf_chance
    avg_dice_damage = (1 + dice_size) / 2
    return avg_dice_damage * avg_chance + gwf_avg * gwf_chance


def get_dice_tokens(weapon_stats):
    if 'dice' in weapon_stats:
        dice_tokens = weapon_stats['dice'].split("d")
    elif 'type' in weapon_stats:
        weapon_type = weapon_stats['type']
        dice_tokens = WEAPON_DICE_MAP[weapon_type].split("d")
    else:
        raise Exception(f"Unable to determine weapon type for {weapon_stats}")
    return dice_tokens


def get_average_dice_damage(build: dict, weapon_stats: dict, dice_size: int):
    weapon_type = weapon_stats['type']

    if 'great-weapon-fighting' in build.get('fighting_styles', []):
        if weapon_type in TWO_HANDED_WEAPONS:
            return apply_great_weapon_fighting(dice_size)
        if empty_offhand(build) and weapon_type in VERSATILE_WEAPONS:
            dice_size += 2  # increase dice_size by 2 if versatile and used with an empty offhand
            return apply_great_weapon_fighting(dice_size)

    avg_dice_damage = (1 + dice_size) / 2  # expected damage of 1 weapon dice roll
    return avg_dice_damage


def get_extra_crit_dice(build: dict):
    """Returns the number of extra weapon dice to roll for a crit
    This number increased for melee half-orc and lvl 9 and 13 barbarian"""
    extra_crit_dice = 0
    if is_melee(build):
        if build.get('race', "") == "half-orc":
            extra_crit_dice += 1
        barbarian_levels = get_class_level(build, "barbarian")
        if barbarian_levels >= 9:
            extra_crit_dice += 1
        if barbarian_levels >= 13:
            extra_crit_dice += 1
        if barbarian_levels >= 17:
            extra_crit_dice += 1
    return extra_crit_dice


def is_finesse(build: dict, weapon_stats=None):
    if weapon_stats is None:
        weapon_stats = build['weapon_1']
    weapon_type = weapon_stats['type']
    return weapon_type in FINESSE_WEAPONS


def is_bg3(build: dict):
    engine = build.get("engine", None)
    if engine == "bg3":
        return True
    return False


def has_reckless(build: dict) -> bool:
    if is_melee(build) and get_class_level(build, "barbarian") >= 2:
        return True
    return False


def is_weapon_type(build: dict, weapon=1, weapon_type="ranged"):
    weapon_str = f"weapon_{weapon}"
    weapon = build.get(weapon_str, None)
    if weapon:
        sub_type = weapon.get("sub_type", None)
        if sub_type == weapon_type:
            return True
    return False


def is_melee(build: dict, weapon=1):
    return is_weapon_type(build, weapon, weapon_type="melee")


def is_ranged(build: dict, weapon=1):
    return is_weapon_type(build, weapon, weapon_type="ranged")


def get_attack_bonus(build: dict, weapon: dict):
    """
    Determine the bonus to hit chance for this attack
    :param build:
    :param weapon:
    :return:
    """
    class_levels = build['class_levels']
    character_level = sum(class_levels.values())
    proficiency = PROFICIENCY_MAP[character_level]
    sub_type = weapon['sub_type']
    fighting_styles = set(build.get("fighting_styles", []))
    bonus = 0
    if sub_type == "ranged" and "archery" in fighting_styles:
        bonus = bonus + 2
    return proficiency + weapon['modifier'] + bonus + build['main_stat_mod']


def attack_damage(build: dict, attacks: int, extra_damage: int = 0, miss_chance: float = .05,
                  crit_chance: float = .05, bonus=False):
    """
    Calculates the expected per-round damage of a weapon
    :param build: Dict containing build information
    :param attacks: number of attacks with provided weapon per round
    :param extra_damage: stat_mod for this attack, and other class bonuses (e.g. rage)
    :param miss_chance: % chance this attack misses
    :param crit_chance: % chance this attack crits
    :param bonus: True if using bonus action attack
    :return:
    """

    weapon_stats = build.get("weapon_1")

    if bonus:
        weapon_stats = get_bonus_weapon(build)

    if weapon_stats is None or len(weapon_stats) == 0:
        return 0  # no weapon, no damage

    dice_tokens = get_dice_tokens(weapon_stats)
    dice_count = int(dice_tokens[0])
    dice_size = int(dice_tokens[1])
    avg_dice_damage = get_average_dice_damage(build, weapon_stats, dice_size)
    weapon_mod_damage = weapon_stats['modifier']
    ttl_mod = weapon_mod_damage + extra_damage
    hit_chance = (1 - crit_chance) - miss_chance
    avg_hit = avg_dice_damage * dice_count + ttl_mod  # expected damage of 1 weapon HIT
    extra_crit_dice = get_extra_crit_dice(build)
    avg_crit = avg_dice_damage * (dice_count * 2 + extra_crit_dice) + ttl_mod
    expected = avg_hit * hit_chance + avg_crit * crit_chance
    return expected * attacks


def get_sneak_dice(build: dict):
    """Returns the # of sneak attack dice for a given rogue level"""
    rogue_level = get_class_level(build, class_name="rogue")
    return math.ceil(rogue_level / 2)


def connect_chance_helper(build: dict, attacks, has_advantage=False):
    miss_rate = get_miss_chance(build, has_advantage)
    connect_chance = 1 - miss_rate
    connect_chance = 1 - (connect_chance ** attacks)
    return connect_chance


def get_connect_chance(build: dict, advantage_attacks, regular_attacks):
    advantage_connect_chance = connect_chance_helper(build, advantage_attacks, has_advantage=True)
    regular_connect_chance = connect_chance_helper(build, regular_attacks, has_advantage=True)
    return advantage_connect_chance + (1 - advantage_connect_chance) * regular_connect_chance


def get_savage_attacker_damage(build: dict, advantage_attacks, regular_attacks):
    """
    Returns the expected damage from savage attack per round
    :param regular_attacks: Number of attacks w/o advantage
    :param advantage_attacks: Number of attacks with advantage
    :param build:
    :return:
    """

    # Savage Attacker only works with melee
    if not is_melee(build) or "savage-attacker" not in get_feats(build):
        return 0

    # Chance that at least one shot connects and we can savage
    connect_chance = get_connect_chance(build, advantage_attacks, regular_attacks)
    crit_chance = connect_chance * get_crit_range(build) * .05
    hit_chance = connect_chance - crit_chance
    weapon_stats = build.get("weapon_1")
    dice_tokens = get_dice_tokens(weapon_stats)
    dice_count = int(dice_tokens[0])
    dice_size = int(dice_tokens[1])
    average = get_average_dice_damage(build, weapon_stats, dice_size)
    diff_damage = (dice_size - average) / 2
    expected_damage = diff_damage * dice_count
    hit_dmg = expected_damage * hit_chance
    crit_damage = expected_damage * 2 * crit_chance
    return hit_dmg + crit_damage  # add amount of damage expected from hit and crit


def get_sneak_damage(build: dict, total_attacks, miss_rate, crit_rate):
    """
    Returns the expected damage from sneak attack per round
    :param build:
    :param total_attacks:
    :param miss_rate:
    :param crit_rate:
    :return:
    """

    # Without advantage; cannot gain sneak attack
    if not has_reckless(build):
        return 0

    if is_melee(build) and not is_finesse(build):
        return 0

    # Chance that at least one shot connects:
    sneak_dice = get_sneak_dice(build)  # once per turn damage
    connect_chance = 1 - miss_rate
    connect_chance = 1 - (connect_chance ** total_attacks)

    crit_chance = connect_chance * crit_rate
    hit_chance = connect_chance - crit_chance
    sneak_average = sneak_dice * (1 + 6) / 2
    sneak_hit = sneak_average * hit_chance
    sneak_crit = sneak_average * 2 * crit_chance
    return sneak_hit + sneak_crit  # add amount of damage expected from hit and crit


def get_crit_range(build):
    crit_range = 1
    if "champion" in build.get("subclasses", []):
        crit_range = 2
        if get_class_level(build, "fighter") >= 15:
            crit_range = 3
    return crit_range


def get_crit_chance(build, has_advantage):
    crit_chance = get_crit_range(build) * .05
    if is_lucky(build):
        crit_chance += .05 * crit_chance

    if has_advantage:
        crit_chance = crit_chance + (1 - crit_chance) * crit_chance
    return round(crit_chance, 10)


def get_feats(build):
    feats = build.get("feats", {})
    build['feats'] = feats
    return feats


def is_lucky(build):
    return build.get("race", "") == "halfling"


def get_miss_chance(build: dict, has_advantage):
    target_ac = build['target_ac']
    hit_bonus = build['hit_bonus']
    miss_rolls = target_ac - hit_bonus - 1  # must roll this number
    crit_range = get_crit_range(build)

    if miss_rolls > (20 - crit_range):
        miss_rolls = 20 - crit_range  # crits always hit
    if miss_rolls < 1:
        miss_rolls = 1  # roll 1 guaranteed miss

    miss_chance = round(miss_rolls * .05, 10)

    # At this point, miss chance is .5% - 95%
    if is_lucky(build):
        connect_chance = round((1 - miss_chance) + 0.05 * (1 - miss_chance), 10)
        miss_chance = 1 - connect_chance

    if has_advantage:
        miss_chance = miss_chance * miss_chance

    return round(miss_chance, 10)


def get_hit_chance(crit_chance: float, miss_chance: float):
    """Gets the % chance to 'hit' on an attack against a specified AC"""
    return (1 - crit_chance) - miss_chance


def get_burst_attacks(build: dict) -> int:
    """
    Returns the number of extra 'burst' attacks this build has
    :param build:
    :return:
    """
    total = 0
    fighter_levels = get_class_level(build, "fighter")
    if fighter_levels >= 2:
        total += get_mh_attacks(build)

    if "gloom-stalker" in build.get("subclasses", []):
        total += 1
    return total


def has_mark(build: dict):
    if get_class_level(build, "ranger") >= 2:
        return True
    if get_class_level(build, "warlock") >= 1:
        return True
    if "oath-of-vengeance" in build.get("subclasses", []):
        return True
    return False


def has_rage(build: dict):
    """
    Returns 'true' if user is melee and can rage
    :param build:
    :return:
    """
    if get_class_level(build, "barbarian") >= 1 and is_melee(build):
        return True
    return False


def get_prep_cost(build: dict) -> int:
    """
    Returns True if the build requires using a bonus action to prep.
    E.g. Activate Rage or use Hunter's Mark
    :param build:
    :return:
    """
    if has_rage(build):
        return 1
    if has_mark(build):
        return 1
    return 0


def get_smite_passive_damage(build: dict, attacks, miss_chance, crit_chance) -> float:
    paladin_levels = get_class_level(build, "paladin")
    if paladin_levels >= 11 and is_melee(build):
        mark_avg = (1 + 8) / 2
        hit_chance = (1 - miss_chance) - crit_chance
        hit_dmg = mark_avg * attacks * hit_chance
        crit_dmg = mark_avg * attacks * crit_chance * 2
        return hit_dmg + crit_dmg
    return 0


def get_gloom_stalker_extra_d8(build: dict, miss_chance, crit_chance) -> float:
    if not has_subclass(build, "gloom-stalker"):
        return 0
    avg_dmg = (1 + 8) / 2
    hit_chance = (1 - miss_chance) - crit_chance
    hit_dmg = avg_dmg * hit_chance
    crit_dmg = avg_dmg * crit_chance * 2
    return hit_dmg + crit_dmg


def get_mark_damage(build: dict, attacks, miss_chance, crit_chance) -> float:
    if has_mark(build):
        mark_avg = (1 + 6) / 2
        hit_chance = (1 - miss_chance) - crit_chance
        hit_dmg = mark_avg * attacks * hit_chance
        crit_dmg = mark_avg * attacks * crit_chance * 2
        return hit_dmg + crit_dmg
    return 0


def get_rage_mod(build: dict):
    barb_levels = get_class_level(build, "barbarian")
    if barb_levels >= 16:
        return 4
    if barb_levels >= 9:
        return 3
    if barb_levels >= 1:
        return 2
    return 0


def add_spell_slots(spell_slots, level, new_slots):
    current_amount = spell_slots.get(level, 0)
    spell_slots[level] = current_amount + new_slots
    return spell_slots


def get_warlock_spell_slots(build, spell_slots=None):
    if spell_slots is None:
        spell_slots = {}
    warlock_levels = get_class_level(build, "warlock")

    # Determine amount of warlock spell slots
    spell_count = 0
    if warlock_levels >= 1:
        spell_count += 1
    if warlock_levels >= 2:
        spell_count += 1
    if warlock_levels >= 11:
        spell_count += 1
    if warlock_levels >= 17:
        spell_count += 1

    # Determine spell level
    if warlock_levels >= 3:
        spell_level = 2
    elif warlock_levels >= 5:
        spell_level = 3
    elif warlock_levels >= 7:
        spell_level = 4
    elif warlock_levels >= 9:
        spell_level = 5
    else:
        spell_level = 1

    if spell_count > 0:
        add_spell_slots(spell_slots, spell_level, spell_count)
    return spell_slots


def get_martial_spell_slots(build, class_name, spell_slots=None):
    if spell_slots is None:
        spell_slots = {}
    class_levels = get_class_level(build, class_name)
    if class_levels >= 2:
        add_spell_slots(spell_slots, 1, 2)
    if class_levels >= 3:
        add_spell_slots(spell_slots, 1, 1)
    if class_levels >= 5:
        add_spell_slots(spell_slots, 1, 1)
        add_spell_slots(spell_slots, 2, 2)
    if class_levels >= 7:
        add_spell_slots(spell_slots, 2, 1)
    if class_levels >= 9:
        add_spell_slots(spell_slots, 3, 2)
    if class_levels >= 11:
        add_spell_slots(spell_slots, 3, 1)
    if class_levels >= 13:
        add_spell_slots(spell_slots, 4, 1)
    if class_levels >= 15:
        add_spell_slots(spell_slots, 4, 1)
    if class_levels >= 17:
        add_spell_slots(spell_slots, 4, 1)
        add_spell_slots(spell_slots, 5, 1)
    if class_levels >= 19:
        add_spell_slots(spell_slots, 5, 1)

    return spell_slots


# Returns the level and amount of spell slots
def get_spell_slots(build: dict):
    spell_slots = {}
    spell_slots = get_martial_spell_slots(build, "paladin", spell_slots=spell_slots)
    spell_slots = get_martial_spell_slots(build, "ranger", spell_slots=spell_slots)
    spell_slots = get_warlock_spell_slots(build, spell_slots=spell_slots)
    # TODO: add spell slots for other spell casting classes
    return spell_slots


def get_highest_slot(spell_slots: dict):
    """Returns the highest spell slot remaining"""
    while len(spell_slots) > 0:
        keys = spell_slots.keys()
        highest_slot = max(keys)
        spell_slots[highest_slot] = spell_slots[highest_slot] - 1
        if spell_slots[highest_slot] == 0:
            del spell_slots[highest_slot]
        return highest_slot
    return 0


def get_burst_smite_damage(build: dict, attacks, miss_chance, crit_chance, spell_slots) -> float:
    paladin_levels = get_class_level(build, "paladin")
    total_smite_dmg = 0
    if paladin_levels >= 2 and is_melee(build):
        for _ in range(0, attacks):
            best_slot = get_highest_slot(spell_slots)
            if best_slot == 0:
                break

            dice_count = 2 + best_slot - 1
            smite_avg = (1 + 8) / 2
            hit_chance = (1 - miss_chance) - crit_chance
            hit_dmg = smite_avg * dice_count * hit_chance
            crit_dmg = smite_avg * dice_count * crit_chance * 2
            total_smite_dmg += (hit_dmg + crit_dmg)

    return total_smite_dmg


def has_subclass(build, subclass):
    subclasses = build.get('subclasses', [])
    return subclass in subclasses


def has_power_attack(build: dict):
    """
    Return 'True' if user attacks are eligible for power attack
    :param build:
    :return:
    """
    if is_ranged(build) and "sharpshooter" in get_feats(build):
        return True
    weapon = build['weapon_1']
    weapon_type = weapon['type']
    if weapon_type in TWO_HANDED_WEAPONS and "great-weapon-master" in get_feats(build):
        return True
    return False


def get_advantage_attacks(build: dict, mh_attacks, b_attacks, extra_bonuses, burst=False) -> int:
    """
    Returns the number of attacks that will have advantage
    :param burst: True if this is a burst round, one free stealth attack for ranged
    :param extra_bonuses: Number of extra bonus actions not being used for attacks
    :param b_attacks: Number of bonus action attacks
    :param mh_attacks: Number of attacks per Attack Action
    :param build:
    :return:
    """
    if has_reckless(build):
        return mh_attacks + b_attacks

    if is_ranged(build):
        advantage_attacks = 0
        if burst:
            advantage_attacks += 1
        if get_class_level(build, "rogue") >= 2:  # can bonus action hide
            advantage_attacks += extra_bonuses
        return min(advantage_attacks, mh_attacks + b_attacks)

    return 0


def get_attack_damage(build: dict, total_attacks, extra_damage, spell_slots, burst=False, no_resources=False,
                      using_rage=False,
                      has_advantage=False, first_attacks=True) -> float:
    """

    :param spell_slots:
    :param has_advantage:
    :param first_attacks:
    :param using_rage:
    :param no_resources:
    :param burst:
    :param extra_damage:
    :param total_attacks:
    :param build:
    :return:
    """
    crit_chance = get_crit_chance(build, has_advantage)
    miss_chance = get_miss_chance(build, has_advantage)
    mark_damage = 0
    smite_damage = 0

    if not no_resources:
        if not using_rage:
            mark_damage = get_mark_damage(build, total_attacks, miss_chance, crit_chance)
        if burst:
            smite_damage = get_burst_smite_damage(build, total_attacks, miss_chance, crit_chance, spell_slots)

    total_damage = 0
    if burst and first_attacks:
        total_damage += get_gloom_stalker_extra_d8(build, miss_chance, crit_chance)

    total_damage += attack_damage(build, total_attacks, extra_damage=extra_damage, miss_chance=miss_chance,
                                  crit_chance=crit_chance, bonus=False)

    if has_advantage:
        total_damage += get_sneak_damage(build, total_attacks, miss_chance, crit_chance)

    total_damage += get_smite_passive_damage(build, total_attacks, miss_chance, crit_chance)
    total_damage += (mark_damage + smite_damage)
    return total_damage


def has_loading(build: dict):
    weapon = build['weapon_1']
    weapon_type = weapon['type']
    if weapon_type in ["heavy-crossbow", "hand-crossbow", "crossbow"]:
        return True
    return False


def apply_loading_limit(build: dict, mh_attacks, bonus_attacks):
    if has_loading(build) and 'crossbow-expert' not in build.get("feats", []):
        return min(mh_attacks, 1), 0
    return mh_attacks, bonus_attacks


def get_dps(build: dict, burst=False, prep=False, no_resources=False):
    """
    Assumes a round 1 using Action Surge; Smiting; Raging, Hunter's Mark, etc.
    :param no_resources:
    :param build:
    :param burst: True if bust attacks should be added
    :param prep:  True if prep costs should be subtracted
    :return:
    """
    stat_mod = build['main_stat_mod']
    mh_attacks = get_mh_attacks(build)
    if burst:
        mh_attacks += get_burst_attacks(build)

    bonus_actions = 1
    if is_bg3(build) and has_subclass(build, "thief"):
        bonus_actions += 1

    b_attacks = get_bonus_attacks(build, no_resources=no_resources)  # number of bonus attacks this class can make
    unpaid_prep = False
    if (prep or burst) and not no_resources:
        prep_cost = get_prep_cost(build)  # either 1 or 0
        bonus_actions -= prep_cost  # take prep costs out of bonus actions
        if bonus_actions < 0:  # No way to trigger this atm; as rage/mark are incompatible
            unpaid_prep = True
            bonus_actions = 0
        b_attacks = max(b_attacks - prep_cost, 0)

    extra_bonuses = max(bonus_actions - b_attacks, 0)
    b_attacks = max(b_attacks, 0)
    mh_attacks, b_attacks = apply_loading_limit(build, mh_attacks, b_attacks)
    total_attacks = mh_attacks + b_attacks
    advantage_attacks = get_advantage_attacks(build, mh_attacks, b_attacks, extra_bonuses, burst=burst)
    regular_attacks = total_attacks - advantage_attacks

    extra_damage = stat_mod + build.get("bonus_hit_damage", 0)
    if "dueling" in build.get("fighting_styles", []) and is_melee(build) and empty_offhand(build):
        extra_damage += 2

    using_rage = has_rage(build) and not unpaid_prep and not no_resources
    if using_rage:
        extra_damage += get_rage_mod(build)

    total_damage = 0
    total_damage += get_savage_attacker_damage(build, total_attacks, regular_attacks)

    spell_slots = get_spell_slots(build)

    total_damage += get_attack_damage(build, advantage_attacks, extra_damage, spell_slots, burst=burst,
                                      no_resources=no_resources,
                                      using_rage=using_rage, has_advantage=True, first_attacks=True)

    total_damage += get_attack_damage(build, regular_attacks, extra_damage, spell_slots, burst=burst,
                                      no_resources=no_resources,
                                      using_rage=using_rage, has_advantage=False,
                                      first_attacks=(advantage_attacks == 0))

    return total_damage


def burst_dps(build: dict):
    return get_dps(build, burst=True)


def prep_dps(build: dict):
    return get_dps(build, burst=False, prep=True)


def sustained_dps(build: dict):
    return get_dps(build, burst=False, prep=False)


def no_resources_dps(build: dict):
    return get_dps(build, burst=False, prep=False, no_resources=True)


def simulate_dps(build: dict, min_ac=17, max_ac=25):
    if min_ac is None:
        proficiency = PROFICIENCY_MAP[get_character_level(build)]
        min_ac = build.get("target_ac", min_ac)
        if min_ac is None:
            min_ac = 13 + proficiency + 3

    if max_ac is None:
        max_ac = min_ac

    sim_range = max((max_ac + 1) - min_ac, 1)
    burst_ttl = 0
    prep_ttl = 0
    sustained_ttl = 0
    no_resources_ttl = 0
    has_advantage = has_reckless(build)
    crit_chance = get_crit_chance(build, has_advantage)
    average_miss_chance = 0
    average_hit_chance = 0
    mh_attacks = 1
    b_attacks = 0
    power_attack = has_power_attack(build)
    power_wasted_ac = ""

    for target_ac in range(min_ac, max_ac + 1):
        build['target_ac'] = target_ac
        mh_attacks = get_mh_attacks(build)
        b_attacks = get_bonus_attacks(build)
        hit_bonus = get_attack_bonus(build, build['weapon_1'])
        build['hit_bonus'] = hit_bonus
        build['bonus_hit_damage'] = 0

        sustained_damage = sustained_dps(build)

        # stop using power attack once it provides lower damage
        if has_power_attack(build) and power_wasted_ac == "":
            hit_bonus = build['hit_bonus'] - 5
            build['hit_bonus'] = hit_bonus
            build['bonus_hit_damage'] = 10
            power_sustained = sustained_dps(build)

            if power_sustained < sustained_damage:
                # Power attack damage is worse; don't use power attacks
                if power_wasted_ac == "":
                    power_wasted_ac = target_ac

                build['hit_bonus'] += 5
                build['bonus_hit_damage'] = 0
            else:
                sustained_damage = power_sustained

        build['hit_bonus'] = hit_bonus
        average_miss_chance += get_miss_chance(build, has_advantage)
        sustained_ttl += sustained_damage
        miss_chance = get_miss_chance(build, has_advantage)
        burst_ttl += burst_dps(build)
        prep_ttl += prep_dps(build)
        no_resources_ttl += no_resources_dps(build)
        average_hit_chance += get_hit_chance(crit_chance=crit_chance, miss_chance=miss_chance)

    burst_avg = burst_ttl / sim_range
    prep_avg = prep_ttl / sim_range
    sustained_avg = sustained_ttl / sim_range
    no_cost_avg = no_resources_ttl / sim_range
    hit_chance = average_hit_chance / sim_range
    miss_chance = average_miss_chance / sim_range
    avg_dps = round((burst_avg + prep_avg + sustained_avg) / 3, 2)

    return {
        "build": build['build_name'],
        "avg": avg_dps,
        "burst": round(burst_avg, 2),
        "prep": round(prep_avg, 2),
        "regular": round(sustained_avg, 2),
        "0-cost": round(no_cost_avg, 2),
        "Min_AC": min_ac,
        "Max_AC": max_ac,
        "+hit": get_attack_bonus(build, build['weapon_1']),
        "crit_range": get_crit_range(build),
        "AHit%": f"{round(hit_chance * 100, 2)}%",
        "AMiss%": f"{round(miss_chance * 100, 2)}%",
        "MH_Atks": mh_attacks,
        "B_Atks": b_attacks,
        "power": power_attack,
        "PW_AC": power_wasted_ac,
        "melee": is_melee(build)
    }


def start_simulation(source="resources/", min_ac=10, max_ac=None, expected_level=None):
    dps_results = []
    for path, sub_dirs, files in os.walk(source):
        for file_name in files:
            class_file = os.path.join(path, file_name)
            build_name = file_name.split(".")[0]
            if not class_file.endswith(".json"):  # only read json files
                continue
            class_file = open(class_file, "r")
            build = class_file.read()
            build = validate_build(build, build_name, expected_level)
            simulation_data = simulate_dps(build, min_ac=min_ac, max_ac=max_ac)
            dps_results.append(simulation_data)
    return dps_results


# Press the green button :in the gutter to run the script.
if __name__ == '__main__':
    directory = "resources/ranged/"
    results = start_simulation(source=directory, min_ac=15, max_ac=27, expected_level=12)
    if len(results) > 0:
        df = pd.DataFrame(results)
        df = df.sort_values("avg", ascending=False)
        df.style.set_properties(**{'text-align': 'left'})
        print(tabulate(df, showindex=False, headers=df.columns))
        df.to_csv(f"{directory}/results.csv")
    else:
        print(f"No results in source:{directory}")
